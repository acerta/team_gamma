import pickle
import pandas as pd

import streamlit as st
import rec_sys
import numpy as np



html = """
	<style>
	.sidebar .sidebar-content {
		background-image: linear-gradient(#33ccff 0%, #ff99ff 100%);
		color: white;
	}
	</style>
	"""

st.markdown(html, unsafe_allow_html=True)


menu = ['Welcome', 'Operation Cinephile']
with st.sidebar.beta_expander('Menu', expanded=False):
    option = st.selectbox('Choose', menu)

if option == 'Welcome':
    st.title('FILM RECOMMENDATION SYSTEM')
    st.subheader('Welcome to Team-Gamma\'s movie recommendation system which takes a string to recommend movies similar to that entered by the user.')
    st.image('images/img1.jpg')
    st.write('Here, the movies are recommended using - Nearest Neighbors clustering algorithm or the similarity score.')
    st.write('The Nearest Neighbors clustering algorithm is an unsupervised algorithm which finds training samples nearest to the input using specified distance metric.')

elif option == 'Operation Cinephile':
    st.title('Operation Cinephile')

    rec_sys.download_s3_folder()
    ratings, movies, tags = rec_sys.ingest_data()
    df_train, ratings_pivot = rec_sys.preprocess(ratings)
    model = rec_sys.train(df_train)

    entered_movie = st.text_input("Please provide a tag for movie recommendation")
    if entered_movie != '':
        output = rec_sys.get_movie_recommendation(df_train,
                                        entered_movie,
                                          model=model,
                                          movies=movies,
                                          ratings_pivot=ratings_pivot)
        st.text("Your recommendations: \n")
        st.text(output)


