import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors
import boto3
import os

import boto3
s3 = boto3.resource('s3') # assumes credentials & configuration are handled outside python in .aws directory or environment variables

def download_s3_folder(bucket_name='data-science-exchange', 
                       s3_folder="team-gamma/data/", 
                       local_dir="data/"):
    """
    Download the contents of a folder directory
    Args:
        bucket_name: the name of the s3 bucket
        s3_folder: the folder path in the s3 bucket
        local_dir: a relative or absolute directory path in the local file system
    """
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_folder):
        target = obj.key if local_dir is None \
            else os.path.join(local_dir, os.path.relpath(obj.key, s3_folder))
        if not os.path.exists(os.path.dirname(target)):
            os.makedirs(os.path.dirname(target))
        if obj.key[-1] == '/':
            continue
        bucket.download_file(obj.key, target)

def ingest_data():
    ratings = pd.read_csv("data/ratings.csv")
    movies = pd.read_csv("data/movies.csv")
    tags = pd.read_csv("data/tags.csv")
    return ratings, movies, tags

def preprocess(ratings):
    ratings_pivot = ratings.pivot(index='movieId',columns='userId',values='rating')
    ratings_pivot = ratings_pivot.fillna(0)
    df_train = csr_matrix(ratings_pivot.values)
    return df_train, ratings_pivot

def train(df_train):
    model = NearestNeighbors(metric='cosine', algorithm='brute', n_neighbors=20, n_jobs=-1)
    model.fit(df_train)
    return model

def get_movie_recommendation(df_train,movie_name, model, movies, ratings_pivot):
    ratings_pivot = ratings_pivot.reset_index()
    n_movies_to_reccomend = 10
    movie_list = movies[movies['title'].str.contains(movie_name)]  
    if len(movie_list):        
        movie_idx= movie_list.iloc[0]['movieId']
        movie_idx = ratings_pivot[ratings_pivot['movieId'] == movie_idx].index[0]
        distances , indices = model.kneighbors(df_train[movie_idx],n_neighbors=n_movies_to_reccomend+1)    
        rec_movie_indices = sorted(list(zip(indices.squeeze().tolist(),distances.squeeze().tolist())),key=lambda x: x[1])[:0:-1]
        recommend_frame = []
        for val in rec_movie_indices:
            movie_idx = ratings_pivot.iloc[val[0]]['movieId']
            idx = movies[movies['movieId'] == movie_idx].index
            recommend_frame.append({'Title':movies.iloc[idx]['title'].values[0]})
        df = pd.DataFrame(recommend_frame,index=range(1,n_movies_to_reccomend+1))
        return df
    else:
        return "No movies found. Please check your input"
    
if __name__ == '__main__':
    download_s3_folder()
    ratings, movies, tags = ingest_data()
    df_train, ratings_pivot = preprocess(ratings)
    model = train(df_train)

    print("Input Movie: ")
    entered_movie = input()
    output = get_movie_recommendation(df_train,
                                    entered_movie,
                                      model=model,
                                      movies=movies,
                                      ratings_pivot=ratings_pivot)
    print(output)
